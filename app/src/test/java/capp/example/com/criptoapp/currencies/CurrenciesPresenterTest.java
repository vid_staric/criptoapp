package capp.example.com.criptoapp.currencies;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import capp.example.com.criptoapp.data.source.CoinMarketRepository;
import capp.example.com.criptoapp.util.Schedulers.SchedulersInstant;
import capp.example.com.criptoapp.util.Schedulers.SchedulersUtil;
import io.reactivex.Flowable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by vid on 2/16/18.
 */
public class CurrenciesPresenterTest {

    @Mock
    private CoinMarketRepository mRepository;
    @Mock
    private CurrenciesContract.View mView;
    private SchedulersUtil mSchedulersUtil;

    private CurrenciesPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mSchedulersUtil = new SchedulersInstant();

        mPresenter = new CurrenciesPresenter(mRepository, mView, mSchedulersUtil);
        mPresenter.setFiat("USD");
        mPresenter.setLimit(100);
    }

    @Test
    public void onCreate_setsPresenterToView() {
        mPresenter = new CurrenciesPresenter(mRepository, mView, mSchedulersUtil);

        verify(mView).setPresenter(mPresenter);
    }


    @Test
    public void onLoad_Empty() {
        when(mRepository.getAllTickers("USD", 100)).thenReturn(Flowable.just(new ArrayList<>()));

        mPresenter.onRefresh();

        verify(mView).showLoading(true);
        verify(mView).showLoading(false);
    }

    @Test
    public void onLoad_Error(){
        when(mRepository.getAllTickers("USD", 100)).thenReturn(Flowable.error(new Exception()));

        mPresenter.onRefresh();

        verify(mView).showLoading(true);
        verify(mView).showLoading(false);
    }
}