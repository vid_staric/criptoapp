package capp.example.com.criptoapp.data.source.remote;

import java.util.List;

import capp.example.com.criptoapp.data.CryptoCurrency;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by vid on 2/15/18.
 */

public interface CoinMarketService {

    @GET("ticker/")
    Flowable<List<CryptoCurrency>> getAll(@Query("convert") String fiatCurrency, @Query("limit") Integer limit);

    @GET("ticker/{id}/")
    Flowable<List<CryptoCurrency>> getById(@Path("id") String id, @Query("convert") String fiatCurrency);
}
