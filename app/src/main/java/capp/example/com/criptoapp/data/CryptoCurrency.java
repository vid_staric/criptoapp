package capp.example.com.criptoapp.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by vid on 2/15/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "symbol",
        "rank",
        "price_btc",
        "available_supply",
        "total_supply",
        "max_supply",
        "percent_change_1h",
        "percent_change_24h",
        "percent_change_7d",
        "last_updated",
        "price_usd",
        "24h_volume_usd",
        "market_cap_usd",
        "price_eur",
        "24h_volume_eur",
        "market_cap_eur",
        "price_cny",
        "24h_volume_cny",
        "market_cap_cny"
})
public class CryptoCurrency {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("rank")
    private String rank;
    @JsonProperty("price_usd")
    private String priceUsd;
    @JsonProperty("price_btc")
    private String priceBtc;
    @JsonProperty("24h_volume_usd")
    private String _24hVolumeUsd;
    @JsonProperty("market_cap_usd")
    private String marketCapUsd;
    @JsonProperty("available_supply")
    private String availableSupply;
    @JsonProperty("total_supply")
    private String totalSupply;
    @JsonProperty("max_supply")
    private String maxSupply;
    @JsonProperty("percent_change_1h")
    private String percentChange1h;
    @JsonProperty("percent_change_24h")
    private String percentChange24h;
    @JsonProperty("percent_change_7d")
    private String percentChange7d;
    @JsonProperty("last_updated")
    private String lastUpdated;
    @JsonProperty("price_eur")
    private String priceEur;
    @JsonProperty("24h_volume_eur")
    private String _24hVolumeEur;
    @JsonProperty("market_cap_eur")
    private String marketCapEur;
    @JsonProperty("price_cny")
    private String priceCny;
    @JsonProperty("24h_volume_cny")
    private String _24hVolumeCny;
    @JsonProperty("market_cap_cny")
    private String marketCapCny;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("rank")
    public String getRank() {
        return rank;
    }

    @JsonProperty("rank")
    public void setRank(String rank) {
        this.rank = rank;
    }

    @JsonProperty("price_usd")
    public String getPriceUsd() {
        return priceUsd;
    }

    @JsonProperty("price_usd")
    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    @JsonProperty("price_btc")
    public String getPriceBtc() {
        return priceBtc;
    }

    @JsonProperty("price_btc")
    public void setPriceBtc(String priceBtc) {
        this.priceBtc = priceBtc;
    }

    @JsonProperty("24h_volume_usd")
    public String get24hVolumeUsd() {
        return _24hVolumeUsd;
    }

    @JsonProperty("24h_volume_usd")
    public void set24hVolumeUsd(String _24hVolumeUsd) {
        this._24hVolumeUsd = _24hVolumeUsd;
    }

    @JsonProperty("market_cap_usd")
    public String getMarketCapUsd() {
        return marketCapUsd;
    }

    @JsonProperty("market_cap_usd")
    public void setMarketCapUsd(String marketCapUsd) {
        this.marketCapUsd = marketCapUsd;
    }

    @JsonProperty("available_supply")
    public String getAvailableSupply() {
        return availableSupply;
    }

    @JsonProperty("available_supply")
    public void setAvailableSupply(String availableSupply) {
        this.availableSupply = availableSupply;
    }

    @JsonProperty("total_supply")
    public String getTotalSupply() {
        return totalSupply;
    }

    @JsonProperty("total_supply")
    public void setTotalSupply(String totalSupply) {
        this.totalSupply = totalSupply;
    }

    @JsonProperty("max_supply")
    public String getMaxSupply() {
        return maxSupply;
    }

    @JsonProperty("max_supply")
    public void setMaxSupply(String maxSupply) {
        this.maxSupply = maxSupply;
    }

    @JsonProperty("percent_change_1h")
    public String getPercentChange1h() {
        return percentChange1h;
    }

    @JsonProperty("percent_change_1h")
    public void setPercentChange1h(String percentChange1h) {
        this.percentChange1h = percentChange1h;
    }

    @JsonProperty("percent_change_24h")
    public String getPercentChange24h() {
        return percentChange24h;
    }

    @JsonProperty("percent_change_24h")
    public void setPercentChange24h(String percentChange24h) {
        this.percentChange24h = percentChange24h;
    }

    @JsonProperty("percent_change_7d")
    public String getPercentChange7d() {
        return percentChange7d;
    }

    @JsonProperty("percent_change_7d")
    public void setPercentChange7d(String percentChange7d) {
        this.percentChange7d = percentChange7d;
    }

    @JsonProperty("last_updated")
    public String getLastUpdated() {
        return lastUpdated;
    }

    @JsonProperty("last_updated")
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @JsonProperty("price_eur")
    public String getPriceEur() {
        return priceEur;
    }

    @JsonProperty("price_eur")
    public void setPriceEur(String priceEur) {
        this.priceEur = priceEur;
    }

    @JsonProperty("24h_volume_eur")
    public String get24hVolumeEur() {
        return _24hVolumeEur;
    }

    @JsonProperty("24h_volume_eur")
    public void set24hVolumeEur(String _24hVolumeEur) {
        this._24hVolumeEur = _24hVolumeEur;
    }

    @JsonProperty("market_cap_eur")
    public String getMarketCapEur() {
        return marketCapEur;
    }

    @JsonProperty("market_cap_eur")
    public void setMarketCapEur(String marketCapEur) {
        this.marketCapEur = marketCapEur;
    }

    @JsonProperty("price_cny")
    public String getPriceCny() {
        return priceCny;
    }

    @JsonProperty("price_cny")
    public void setPriceCny(String priceCny) {
        this.priceCny = priceCny;
    }

    @JsonProperty("24h_volume_cny")
    public String get24hVolumeCny() {
        return _24hVolumeCny;
    }

    @JsonProperty("24h_volume_cny")
    public void set24hVolumeCny(String _24hVolumeCny) {
        this._24hVolumeCny = _24hVolumeCny;
    }

    @JsonProperty("market_cap_cny")
    public String getMarketCapCny() {
        return marketCapCny;
    }

    @JsonProperty("market_cap_cny")
    public void setMarketCapCny(String marketCapCny) {
        this.marketCapCny = marketCapCny;
    }
}