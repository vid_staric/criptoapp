package capp.example.com.criptoapp.data.source;

import java.util.List;

import capp.example.com.criptoapp.data.CryptoCurrency;
import io.reactivex.Flowable;

/**
 * Created by vid on 2/15/18.
 */

public interface CoinMarketDataSource {

    Flowable<List<CryptoCurrency>> getAllTickers(String fiatCurrency, Integer limit);

    Flowable<CryptoCurrency> getCurrencyTicker(String id, String fiatCurrency);
}
