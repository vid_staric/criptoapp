package capp.example.com.criptoapp.currencyDetails;

import capp.example.com.criptoapp.BasePresenter;
import capp.example.com.criptoapp.BaseView;

/**
 * Created by vid on 2/15/18.
 */

public interface CurrenciesDetailsContract {
    interface View extends BaseView<Presenter> {

        void showLoading(boolean show);

        void displayItem(String parsedString);
    }

    interface Presenter extends BasePresenter {
        void onRefresh();
    }
}
