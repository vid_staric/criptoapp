package capp.example.com.criptoapp.currencyDetails;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import capp.example.com.criptoapp.R;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesDetailsFragment extends Fragment implements CurrenciesDetailsContract.View {

    private CurrenciesDetailsContract.Presenter mPresenter;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.all_data)
    TextView mAllData;

    public CurrenciesDetailsFragment() {
    }

    public static CurrenciesDetailsFragment newInstance() {
        return new CurrenciesDetailsFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_currency_details, container, false);
        ButterKnife.bind(this, view);

        mSwipeRefresh.setOnRefreshListener(() -> mPresenter.onRefresh());
        return view;
    }

    @Override
    public void setPresenter(CurrenciesDetailsContract.Presenter presenter) {
        mPresenter = presenter;
    }


    @Override
    public void showLoading(boolean show) {
        mSwipeRefresh.setRefreshing(show);
    }

    @Override
    public void displayItem(String parsedString) {
        mAllData.setText(parsedString);
    }
}
