package capp.example.com.criptoapp.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import capp.example.com.criptoapp.R;

/**
 * Created by vid on 2/16/18.
 */

public class FragmentUtil {

    public static <T extends Fragment> T add(T fragment, FragmentManager manager) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.contentFrame, fragment);
        transaction.commit();

        return fragment;
    }
}
