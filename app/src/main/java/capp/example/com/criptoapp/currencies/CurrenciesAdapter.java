package capp.example.com.criptoapp.currencies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import capp.example.com.criptoapp.R;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesAdapter extends BaseAdapter {

    public interface CurrencyItemListener {
        void onItemSelected(String id);
    }

    private List<CryptoCurrencyItem> mItems;
    private CurrencyItemListener mItemListener;

    public CurrenciesAdapter(CurrencyItemListener mItemListener) {
        this.mItems = new ArrayList<>();
        this.mItemListener = mItemListener;
    }

    public void displayItems(List<CryptoCurrencyItem> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CryptoCurrencyItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.view_currencie_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        final CryptoCurrencyItem item = getItem(position);
        holder.rank.setText(item.getRank());
        holder.symbol.setText(item.getSymbol());
        holder.priceFiat.setText(item.getPriceFiat());
        holder.change24H.setText(item.getChange24H());

        view.setOnClickListener(__ -> {
            mItemListener.onItemSelected(item.getId());
        });

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.rank)
        TextView rank;
        @BindView(R.id.symbol)
        TextView symbol;
        @BindView(R.id.price_fiat)
        TextView priceFiat;
        @BindView(R.id.change_24_h)
        TextView change24H;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}