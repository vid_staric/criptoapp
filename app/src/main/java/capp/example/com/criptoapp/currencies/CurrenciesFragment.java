package capp.example.com.criptoapp.currencies;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import capp.example.com.criptoapp.R;
import capp.example.com.criptoapp.currencyDetails.CurrenciesDetailsActivity;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesFragment extends Fragment implements CurrenciesContract.View, CurrenciesAdapter.CurrencyItemListener {

    @BindView(R.id.refresh)
    SwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.list)
    ListView mListView;
    @BindView(R.id.search_by)
    EditText mSearchByText;

    private CurrenciesContract.Presenter mPresenter;
    private CurrenciesAdapter mAdapter;

    public CurrenciesFragment() {
    }

    public static CurrenciesFragment newInstance() {
        return new CurrenciesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new CurrenciesAdapter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_currencies, container, false);
        ButterKnife.bind(this, view);

        mListView.setAdapter(mAdapter);
        mSwipeRefresh.setOnRefreshListener(() -> mPresenter.onRefresh());
        return view;
    }


    @OnTextChanged(value = R.id.search_by, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void afterTextChanged(Editable editable){
        mPresenter.filterByName(editable.toString());
    }

    @Override
    public void setPresenter(CurrenciesContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onItemSelected(String id) {
        Intent intent = new Intent(getActivity(), CurrenciesDetailsActivity.class);
        intent.putExtra(CurrenciesDetailsActivity.CURRENCY_ID, id);
        startActivity(intent);
    }

    @Override
    public void displayItems(List<CryptoCurrencyItem> list) {
        mAdapter.displayItems(list);
    }

    @Override
    public void showLoading(boolean show) {
        mSwipeRefresh.setRefreshing(show);
    }
}
