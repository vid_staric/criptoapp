package capp.example.com.criptoapp.util.Schedulers;


import io.reactivex.Scheduler;

/**
 * Created by vid on 2/16/18.
 */

public interface SchedulersUtil {

    Scheduler getIo();

    Scheduler getUi();
}
