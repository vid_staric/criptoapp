package capp.example.com.criptoapp.currencyDetails;

import com.fasterxml.jackson.databind.ObjectMapper;

import capp.example.com.criptoapp.data.source.CoinMarketRepository;
import capp.example.com.criptoapp.util.Schedulers.SchedulersUtil;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesDetailsPresenter implements CurrenciesDetailsContract.Presenter {

    private final CoinMarketRepository mRepository;
    private final CurrenciesDetailsContract.View mView;
    private CompositeDisposable mCompositeDisposable;
    private final SchedulersUtil mSchedulersUtil;

    private String fiat;
    private String currencyId;

    public CurrenciesDetailsPresenter(CoinMarketRepository mRepository, CurrenciesDetailsContract.View mView, String currencyId, SchedulersUtil mSchedulersUtil) {
        this.mRepository = mRepository;
        this.mView = mView;
        this.currencyId = currencyId;
        this.mSchedulersUtil = mSchedulersUtil;

        mCompositeDisposable = new CompositeDisposable();

        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        getCurrency();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void onRefresh() {
        getCurrency();
    }

    private void getCurrency() {
        mView.showLoading(true);

        Disposable disposable = mRepository.getCurrencyTicker(currencyId, fiat)
                .map(cc -> {
                    ObjectMapper mapper = new ObjectMapper();
                    return mapper.writeValueAsString(cc);
                })
                .map(s -> s.replaceAll("[,{}]", "\n\t"))
                .subscribeOn(mSchedulersUtil.getIo())
                .observeOn(mSchedulersUtil.getUi())
                .subscribe(parsedString -> {
                    mView.displayItem(parsedString);
                    mView.showLoading(false);
                }, Throwable::printStackTrace);

        mCompositeDisposable.add(disposable);

    }

    public void setFiat(String fiat) {
        this.fiat = fiat;
    }
}
