package capp.example.com.criptoapp.data.source.util;

import capp.example.com.criptoapp.data.source.remote.CoinMarketService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by vid on 2/15/18.
 */

public class RetrofitHelper {

    private static final String BASE_URL = "https://api.coinmarketcap.com/v1/";
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(new OkHttpClient())
            .build();

    public static CoinMarketService getCoinMarketService() {
        return retrofit.create(CoinMarketService.class);
    }


}
