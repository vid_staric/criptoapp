package capp.example.com.criptoapp.util;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by vid on 2/15/18.
 */

public class SharedPreferencesUtil {

    private static final String MY_PREFS = "MyPrefs";
    private static final String FIAT = "Fiat";
    private static final String LIMIT = "Limit";

    public static String getFiat(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS, 0);

        return preferences.getString(FIAT, "USD");
    }

    public static int getLimit(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS, 0);

        return preferences.getInt(LIMIT, 100);
    }

    public static void setFiatAndLimit(Context context, String fiat, int limit) {
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFS, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(FIAT, fiat);
        editor.putInt(LIMIT, limit);
        editor.apply();
    }
}
