package capp.example.com.criptoapp.currencies;

import capp.example.com.criptoapp.data.source.CoinMarketRepository;
import capp.example.com.criptoapp.util.Schedulers.SchedulersUtil;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesPresenter implements CurrenciesContract.Presenter {

    private final CoinMarketRepository mRepository;
    private final CurrenciesContract.View mView;
    private final SchedulersUtil mSchedulersUtil;

    private CompositeDisposable mCompositeDisposable;
    private int limit;
    private String fiat;
    private String nameAndSymbol;

    public CurrenciesPresenter(CoinMarketRepository mRepository, CurrenciesContract.View mView, SchedulersUtil mSchedulersUtil) {
        this.mRepository = mRepository;
        this.mView = mView;
        this.mSchedulersUtil = mSchedulersUtil;

        mCompositeDisposable = new CompositeDisposable();
        nameAndSymbol = "";
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        getCurrenciesList();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void onRefresh() {
        getCurrenciesList();
    }

    @Override
    public void filterByName(String name) {
        nameAndSymbol = name.toLowerCase();
        //not the most optimal strategy since we are pulling data from the web
        getCurrenciesList();
    }

    private void getCurrenciesList() {
        //todo tmp hardcoded
        mView.showLoading(true);
        Disposable disposable = mRepository.getAllTickers(fiat, limit)
                .flatMapIterable(criptoCurrencies -> criptoCurrencies)
                .filter(cc -> {
                    if (nameAndSymbol.isEmpty())
                        return true;
                    return cc.getName().toLowerCase().contains(nameAndSymbol) || cc.getSymbol().toLowerCase().contains(nameAndSymbol);
                })
                .map(cc -> new CryptoCurrencyItem(cc, fiat))
                .toList()
                .subscribeOn(mSchedulersUtil.getIo())
                .observeOn(mSchedulersUtil.getUi())
                .subscribe(list -> {
                    mView.displayItems(list);
                    mView.showLoading(false);
                }, e -> {
                    e.printStackTrace();
                    mView.showLoading(false);
                });

        mCompositeDisposable.add(disposable);
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setFiat(String fiat) {
        this.fiat = fiat;
    }
}
