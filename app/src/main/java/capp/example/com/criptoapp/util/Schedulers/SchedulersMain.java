package capp.example.com.criptoapp.util.Schedulers;


import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by vid on 2/16/18.
 */

public class SchedulersMain implements SchedulersUtil {

    private static SchedulersMain INSTANCE;

    private SchedulersMain() {
    }

    public static synchronized SchedulersMain getInstane() {
        if (INSTANCE == null)
            INSTANCE = new SchedulersMain();

        return INSTANCE;
    }

    @Override
    public Scheduler getIo() {
        return io.reactivex.schedulers.Schedulers.io();
    }

    @Override
    public Scheduler getUi() {
        return AndroidSchedulers.mainThread();
    }
}
