package capp.example.com.criptoapp.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import capp.example.com.criptoapp.data.CryptoCurrency;
import io.reactivex.Flowable;

/**
 * Created by vid on 2/15/18.
 */

public class CoinMarketRepository implements CoinMarketDataSource {

    @Nullable
    private static CoinMarketRepository INSTANCE = null;

    @NonNull
    private final CoinMarketDataSource mRemoteSource;


    private CoinMarketRepository(@NonNull CoinMarketDataSource remoteSource) {
        this.mRemoteSource = remoteSource;
    }

    public static CoinMarketRepository getInstance(@NonNull CoinMarketDataSource remoteSource) {
        if (INSTANCE == null)
            INSTANCE = new CoinMarketRepository(remoteSource);

        return INSTANCE;
    }

    @Override
    public Flowable<List<CryptoCurrency>> getAllTickers(String fiatCurrency, Integer limit) {
        return mRemoteSource.getAllTickers(fiatCurrency, limit);
    }

    @Override
    public Flowable<CryptoCurrency> getCurrencyTicker(String id, String fiatCurrency) {
        return mRemoteSource.getCurrencyTicker(id, fiatCurrency);
    }
}
