package capp.example.com.criptoapp.currencies;

import java.util.List;

import capp.example.com.criptoapp.BasePresenter;
import capp.example.com.criptoapp.BaseView;

/**
 * Created by vid on 2/15/18.
 */

public interface CurrenciesContract {
    interface View extends BaseView<Presenter> {

        void displayItems(List<CryptoCurrencyItem> list);

        void showLoading(boolean show);
    }

    interface Presenter extends BasePresenter {

        void onRefresh();

        void filterByName(String name);
    }
}
