package capp.example.com.criptoapp.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import capp.example.com.criptoapp.R;
import capp.example.com.criptoapp.util.SharedPreferencesUtil;

/**
 * Created by vid on 2/15/18.
 */

public class SettingsActivity extends AppCompatActivity {

    public static final int SETTINGS_RES_CODE = 1234;

    @BindView(R.id.rg_fiat_group)
    RadioGroup mFiatGroup;
    @BindView(R.id.seekbar)
    SeekBar mLimitBar;
    @BindView(R.id.display_limit)
    TextView mDisplayLimit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        int limit = SharedPreferencesUtil.getLimit(this);
        String currencyId = SharedPreferencesUtil.getFiat(this);

        switch (currencyId.toUpperCase()) {
            case "EUR":
                mFiatGroup.check(R.id.rb_eur);
                break;
            case "CNY":
                mFiatGroup.check(R.id.rb_cny);
                break;
            case "USD":
            default:
                mFiatGroup.check(R.id.rb_usd);
        }

        mLimitBar.setMax(99);
        mLimitBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mDisplayLimit.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mLimitBar.setProgress(limit - 1);


        mDisplayLimit.setText(String.valueOf(limit));
    }

    @Override
    public void onBackPressed() {
        onDone();
    }

    @OnClick(R.id.done)
    public void onDone() {
        String fiat;
        switch (mFiatGroup.getCheckedRadioButtonId()) {
            case R.id.rb_eur:
                fiat = "EUR";
                break;
            case R.id.rb_cny:
                fiat = "CNY";
                break;
            case R.id.rb_usd:
            default:
                fiat = "USD";
        }

        SharedPreferencesUtil.setFiatAndLimit(this, fiat, mLimitBar.getProgress() + 1);
        setResult(Activity.RESULT_OK);
        finish();
    }


}
