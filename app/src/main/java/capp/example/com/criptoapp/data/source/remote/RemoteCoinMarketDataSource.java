package capp.example.com.criptoapp.data.source.remote;

import java.util.List;

import capp.example.com.criptoapp.data.CryptoCurrency;
import capp.example.com.criptoapp.data.source.CoinMarketDataSource;
import capp.example.com.criptoapp.data.source.util.RetrofitHelper;
import io.reactivex.Flowable;

/**
 * Created by vid on 2/15/18.
 */

public class RemoteCoinMarketDataSource implements CoinMarketDataSource {

    private static RemoteCoinMarketDataSource INSTANCE;

    private RemoteCoinMarketDataSource() {
    }

    public static RemoteCoinMarketDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new RemoteCoinMarketDataSource();

        return INSTANCE;
    }

    @Override
    public Flowable<List<CryptoCurrency>> getAllTickers(String fiatCurrency, Integer limit) {
        return RetrofitHelper.getCoinMarketService().getAll(fiatCurrency, limit);
    }

    @Override
    public Flowable<CryptoCurrency> getCurrencyTicker(String id, String fiatCurrency) {
        return RetrofitHelper.getCoinMarketService().getById(id, fiatCurrency)
                .flatMapIterable(cryptoCurrencies -> cryptoCurrencies)
                .limit(1);
    }
}
