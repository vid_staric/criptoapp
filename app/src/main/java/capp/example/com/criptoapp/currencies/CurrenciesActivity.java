package capp.example.com.criptoapp.currencies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import capp.example.com.criptoapp.R;
import capp.example.com.criptoapp.data.source.CoinMarketRepository;
import capp.example.com.criptoapp.data.source.remote.RemoteCoinMarketDataSource;
import capp.example.com.criptoapp.settings.SettingsActivity;
import capp.example.com.criptoapp.util.FragmentUtil;
import capp.example.com.criptoapp.util.Schedulers.SchedulersMain;
import capp.example.com.criptoapp.util.SharedPreferencesUtil;

/**
 * Created by vid on 2/15/18.
 */

public class CurrenciesActivity extends AppCompatActivity {

    private CurrenciesPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currencies);

        setSupportActionBar(findViewById(R.id.toolbar));

        CurrenciesFragment currenciesFragment = (CurrenciesFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (currenciesFragment == null)
            currenciesFragment = FragmentUtil.add(CurrenciesFragment.newInstance(), getFragmentManager());

        mPresenter = new CurrenciesPresenter(
                CoinMarketRepository.getInstance(RemoteCoinMarketDataSource.getInstance()), currenciesFragment, SchedulersMain.getInstane());
    }

    @Override
    protected void onResume() {
        setFiatAndLimit();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, SettingsActivity.SETTINGS_RES_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SettingsActivity.SETTINGS_RES_CODE) {
            setFiatAndLimit();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setFiatAndLimit() {
        mPresenter.setFiat(SharedPreferencesUtil.getFiat(this));
        mPresenter.setLimit(SharedPreferencesUtil.getLimit(this));
    }
}
