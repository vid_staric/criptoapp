package capp.example.com.criptoapp.util.Schedulers;

import io.reactivex.Scheduler;

/**
 * Created by vid on 2/16/18.
 */

public class SchedulersInstant implements SchedulersUtil {

    @Override
    public Scheduler getIo() {
        return io.reactivex.schedulers.Schedulers.trampoline();
    }

    @Override
    public Scheduler getUi() {
        return io.reactivex.schedulers.Schedulers.trampoline();
    }
}
