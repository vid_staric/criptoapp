package capp.example.com.criptoapp.currencies;

import capp.example.com.criptoapp.data.CryptoCurrency;

/**
 * Created by vid on 2/15/18.
 */

class CryptoCurrencyItem {
    private final String id;
    private final String rank;
    private final String symbol;
    private final String priceFiat;
    private final String change24H;

    public CryptoCurrencyItem(CryptoCurrency cryptoCurrency, String selectedFiat) {
        this.id = cryptoCurrency.getId();
        this.rank = cryptoCurrency.getRank();
        this.symbol = cryptoCurrency.getSymbol();
        switch (selectedFiat.toUpperCase()) {
            case "EUR":
                this.priceFiat = cryptoCurrency.getPriceEur() + " Eur";
                break;
            case "CNY":
                this.priceFiat = cryptoCurrency.getPriceCny()+ " Cny";
                break;
            case "USD":
            default:
                this.priceFiat = cryptoCurrency.getPriceUsd() + " Usd";
        }
        this.change24H = cryptoCurrency.getPercentChange24h();

    }

    public String getRank() {
        return rank;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getPriceFiat() {
        return priceFiat;
    }

    public String getChange24H() {
        return change24H;
    }

    public String getId() {
        return id;
    }
}
