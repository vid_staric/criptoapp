package capp.example.com.criptoapp;

/**
 * Created by vid on 2/15/18.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
